To encrypt a file, drag and drop it onto encrypt.bat.

This will produce msg.txt.enc which you can send to the receiver using any method.
You (the 'sender') can send a file in an encrypted form to whoever gave you this package (the 'receiver').
Once a file is encrypted (with .enc extension), it can only be decrypted by the receiver.
The original file is left untouched.

Find within this folder,
	* encrypt.bat
	* openssl.exe
	* README.txt [This file]
	* public_key.pem [Required to perform encryption]

==================================================================
openssl.exe is GPL'd software.
It is provided in binary form from
http://gnuwin32.sourceforge.net/packages/openssl.htm
sources are also available there.
