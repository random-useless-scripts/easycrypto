This package provides helpful windows batch scripts as a front end to openssl.
I made this to make one-way transmission of files easy for the sender, and painless for the receiver.

It is assumed that the receiver knows the basics of RSA encryption, enough to know NOT to send their generated private key to the sender.

==========
HOW TO USE
==========
Find within this folder,
	- reciever
		* decrypt.bat
		* openssl.exe
		+ private_key.pem [generated file]
	- sender
		* encrypt.bat
		* README.txt
		+ openssl.exe [generated file]
		+ public_key.pem [generated file]
	* generate_key.bat
	* README.txt [This file]

# Run generate_key.bat
This generates a private key under the reciever subdirectory, and the corresponding public key under the sender subdirectory.

# Give the sender subdirectory.

The sender will send you an encrypted file, created by dragging and dropping the file onto sender\encrypt.bat and has the extension '.enc'.

# Decrypt by dragging and dropping the '.enc' file onto reciever\decrypt.bat.
It may be necessary to remove the '.enc.txt' file extension if the original file was not a simple '.txt' file.

==================================================================
openssl.exe is GPL'd software.
It is provided in binary form from
http://gnuwin32.sourceforge.net/packages/openssl.htm
sources are also available there.
